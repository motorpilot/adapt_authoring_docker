# Adapt Authoring Tool - Docker Installation

## Docker

You need to install docker on your system so head on over to the Docker website and [download and install Docker Desktop](https://www.docker.com/get-started) for your platform (Windows/Mac).

If you're on Linux then Docker Desktop is not yet available so head to [Install Docker Engine](https://docs.docker.com/engine/install/) and follow the instructions for your distribution there.

## First Run

Now you've got Docker installed you need to run the following commands from your terminal (make sure you change directory to wherever you downloaded the files from the repository).

This pulls/build the images and creates and starts the containers.
```
docker compose up -d
```

Wait (could take up to take a few minutes) to be returned to the prompt which should be immediately after seeing:
```
Container mongo                              Started
Container adaptauthoring                     Started
```

Now, the Adapt Authoring Tool needs to be installed. This has to be done once the DB is up and running as we can't do this in the build phase.  
We spin up a temporary container in the project that uses the same volumes that were created in the previous step.  
This allows you to install the application via a browser and then removes the temporary container once done.
```
docker compose -f docker-compose.setup.yml run -p 8080:8080 --rm setup
```

Shortly after running this command you should see:
```
Application running.
If the page doesn't open automatically, please visit http://localhost:8080 in your web browser.
```

The page *won't* open automatically so, leaving your terminal running, fire up your preferred browser and head to [http://localhost:8080](http://localhost:8080).

Whilst the application installs you can also follow the output in your terminal if you wish.

Follow the on screen instructions, in your browser, choosing the latest release (or another if you prefer) to download/install.  
When you hit the configuration screen please enter random values (longer than 10 characters) for `tokenSecret` and `secret`.  
For the other configuration items please use the following values:

- `connectionUri`: mongodb://mongo:27017
- `host`: localhost
- `port`: 5050
- `url`: http://localhost:5050/

When you submit the tool will then continue with its installation (again, follow along in the terminal if you wish).

The final screen/step is to add your superUser.  
Enter the requested details and submit to wrap up the installation process.  
You can now close the browser window.

Head back to your terminal and you should find you've been returned to the prompt and one of the last messages output will be:
```
Application installed successfully. To start the app, please run the following commands:

cd /app
npm start
```

You should now be able to access your installation via [http://localhost:5050](http://localhost:5050)!

## Logging in

Head to [http://localhost:5050](http://localhost:5050) and use the (superUser) details you entered during the installation.

Further users, should you need them, can be added via the tool.

## Stopping/Starting

To stop/start (without removing containers/images/volumes) use:
```
docker compose stop
```
```
docker compose start
```

To stop and remove the containers, leaving the images and volumes (so a re-install is not needed), use:
```
docker compose down
```

After which, to start again, use:
```
docker compose up -d
```

## Removing

NB. You will lose all data stored within the DB and saved whilst using the app doing this!

To stop and remove containers, images, and volumes use:
```
docker compose down -rmi all -v
```

After this you will need to return to [First Run](#first-run) and start afresh!