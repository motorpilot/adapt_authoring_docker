FROM node:gallium-bullseye

LABEL org.opencontainers.image.authors="simon.lewis@motorpilot.com"

WORKDIR /app

EXPOSE 5050
CMD npm start